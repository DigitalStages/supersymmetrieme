#pragma once

#include "ofMain.h"
#include "ofxCv.h"

class ofApp : public ofBaseApp {

public:
    void setup();
    void update();
    void draw();
    void snapshot();
    void showtake();
    void slideshow();

    int camW,camH,scrW,scrH,faceX,faceY,faceW,faceH;
    float ratioW,ratioH;
    ofVideoGrabber cam;
    
    ofImage face_L,face_R,img;
    ofxCv::ObjectFinder faceFinder;
    ofxCv::ObjectFinder noseFinder;
    
    unsigned long startTime,curTime,sStartTime,sCurTime,sTimer;
    bool takeIt,showIt,slideIt;
    string countDown,fileName;
    ofTrueTypeFont	font;
    ofImage screenShot;
    
    ofDirectory dir;
    vector<ofImage> images;
    int curImg;
};
