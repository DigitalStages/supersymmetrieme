#include "ofMain.h"
#include "ofApp.h"

//========================================================================
int main( ){		// <-------- setup the GL context
    // this kicks off the running of my app
    // can be OF_WINDOW or OF_FULLSCREEN
    // pass in width and height too:
    ofGLFWWindowSettings settings;
    settings.monitor=2;
    settings.multiMonitorFullScreen = true;
    settings.setSize(2560,1024);
    settings.decorated = false;
    ofCreateWindow(settings);
    return ofRunApp(new ofApp());
}

