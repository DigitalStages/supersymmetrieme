#include "ofApp.h"

using namespace ofxCv;
using namespace cv;


//--------------------------------------------------------------
void ofApp::setup(){

    //try to grab at this size.
    camW = 640;
    camH = 480;

    //start cam with 30 fps if possible
    ofSetFrameRate(120);

    //get mon size in total
    scrW = ofGetWidth()/2;
    scrH = ofGetHeight();

    //get scaleRatio
    ratioW = float(scrW)/ float(camW);
    ratioH = float((scrH-(scrH-(camH*2))))/ float(camH);

    //we can now get back a list of devices.
    vector<ofVideoDevice> devices = cam.listDevices();

    for(int i = 0; i < devices.size(); i++){
        if(devices[i].bAvailable){
            ofLogNotice() << devices[i].id << ": " << devices[i].deviceName;
        }else{
            ofLogNotice() << devices[i].id << ": " << devices[i].deviceName << " - unavailable ";
        }
    }

    cam.setDeviceID(0);
    cam.setDesiredFrameRate(60);
    cam.initGrabber(camW,camH);

    ofSetVerticalSync(true);

    faceFinder.setup(ofToDataPath("haarcascade_frontalface_default.xml"));
    //faceFinder.setPreset(ObjectFinder::Sensitive);
    faceFinder.getTracker().setSmoothingRate(1.0);
    faceFinder.setRescale(0.5); //the size it will rescale the incoming image into
    faceFinder.setMinNeighbors(3);
    faceFinder.setMultiScaleFactor(1.02);
    faceFinder.setMinSizeScale(0.4); //smallest object it will find
    faceFinder.setMaxSizeScale(1); //largest object
    faceFinder.setCannyPruning(false);
    faceFinder.setFindBiggestObject(true);
    face_L.allocate(camW*2,camH*2,OF_IMAGE_COLOR);
    face_R.allocate(camW*2,camH*2,OF_IMAGE_COLOR);

    takeIt = false;
    startTime = ofGetElapsedTimeMillis();
    curTime = 0;
    sStartTime = ofGetElapsedTimeMillis();
    sCurTime = 0;
    slideIt = false;
    ofTrueTypeFont::setGlobalDpi(72);
    font.load("time.ttf",92, true, true);
    countDown = "SuSyMe!";
    fileName = "";
    //Show snapped Image and SlideShow
    showIt = false;
    sTimer = 10000;
    img.allocate(scrW*2,scrH,OF_IMAGE_COLOR);
    //Slideshow
    dir.listDir("/home/susy/Pictures/SuSyMe/");
    dir.allowExt("png");
    dir.sort();
    curImg = 0;
}

//--------------------------------------------------------------
void ofApp::update(){
    ofBackground(0, 0, 0);
    cam.update();
    if(cam.isFrameNew()) {
        faceFinder.update(cam);
        if(faceFinder.size() > 0){
            curTime = ofGetElapsedTimeMillis() - startTime;
        }
    }
}

//--------------------------------------------------------------
void ofApp::draw(){
    //Draw updated camera frame
    cam.draw(scrW,scrH-(camH*2), -camW*2, camH*2);
    cam.draw(scrW*2,scrH-(camH*2), -camW*2, camH*2);
    //Add face halfs
    if(faceFinder.size() > 0 && takeIt == false && showIt == false){
        if(slideIt == true && sCurTime > sTimer){
            slideIt = false;
        }
        else{
            sStartTime = ofGetElapsedTimeMillis();
        }

        for(unsigned int i = 0; i < faceFinder.size(); i++){
            Mat camMat = toCv(cam);
            // Setup a rectangle to define your region of interest
            Rect roi_R(faceFinder.getObject(i).x, faceFinder.getObject(i).y, faceFinder.getObject(i).width/2, faceFinder.getObject(i).height);
            Rect roi_L(faceFinder.getObject(i).x+faceFinder.getObject(i).width/2, faceFinder.getObject(i).y, faceFinder.getObject(i).width/2, faceFinder.getObject(i).height);
            // Crop the full image to that image contained by the rectangle myROI
            // Note that this doesn't copy the data
            Mat half_R = camMat(roi_R);
            Mat half_L = camMat(roi_L);
            //do the cut, crop and mirroring
            resize(half_R, face_R, 1, 1);
            resize(half_L, face_L, 1, 1);
            //update image buffer
            face_R.update();
            face_L.update();
            //draw images from image buffer
            face_R.draw(scrW-(roi_R.x*ratioW+roi_R.width*ratioW*2),(roi_R.y*ratioH)+(scrH-(camH*2)),roi_R.width*ratioW,roi_R.height*ratioH);
            face_L.draw(scrW*2-(roi_L.x*ratioW),(roi_L.y*ratioH)+(scrH-(camH*2)),roi_L.width*ratioW,roi_L.height*ratioH);
            //Start Countdown to snapshot
            if(curTime > 2000 && curTime <= 6000){
                ofSetColor(255,255,255);
                ofPushMatrix();
    		        countDown = "SuSyMe?";
		            ofRectangle bounds = font.getStringBoundingBox(countDown, 0, 0);
                    ofTranslate(scrW/2,scrH/2,0);
	    	        font.drawString(countDown, -bounds.width/2, bounds.height/2 );
                    ofTranslate(scrW,0,0);
                    font.drawString(countDown, -bounds.width/2, bounds.height/2 ); 
    	        ofPopMatrix();
                ofSetColor(255,255,255);
            }
            else if(curTime > 6000 && curTime <= 7000){
                ofSetColor(0,255,0);
                ofPushMatrix();
    		        countDown = "3";
		            ofRectangle bounds = font.getStringBoundingBox(countDown, 0, 0);
                    ofTranslate(scrW/2,scrH/2,0);
	    	        font.drawString(countDown, -bounds.width/2, bounds.height/2 );
                    ofTranslate(scrW,0,0);
                    font.drawString(countDown, -bounds.width/2, bounds.height/2 ); 
    	        ofPopMatrix();
                ofSetColor(255,255,255);
            }
            else if(curTime > 7000 && curTime <= 8000){
                ofSetColor(255,255,0);
                ofPushMatrix();
    		        countDown = "2";
		            ofRectangle bounds = font.getStringBoundingBox(countDown, 0, 0);
                    ofTranslate(scrW/2,scrH/2,0);
	    	        font.drawString(countDown, -bounds.width/2, bounds.height/2 );
                    ofTranslate(scrW,0,0);
                    font.drawString(countDown, -bounds.width/2, bounds.height/2 ); 
    	        ofPopMatrix();
                ofSetColor(255,255,255);
            }
            else if(curTime > 8000 && curTime <= 9000){
                ofSetColor(255,0,0);
                ofPushMatrix();
    		        countDown = "1";
		            ofRectangle bounds = font.getStringBoundingBox(countDown, 0, 0);
                    ofTranslate(scrW/2,scrH/2,0);
	    	        font.drawString(countDown, -bounds.width/2, bounds.height/2 );
                    ofTranslate(scrW,0,0);
                    font.drawString(countDown, -bounds.width/2, bounds.height/2 ); 
    	        ofPopMatrix();
                ofSetColor(255,255,255);
            }
            else if(curTime > 9000 && curTime <= 10000){
                ofSetColor(255,255,255);
                ofPushMatrix();
    		        countDown = "SNAP!";
		            ofRectangle bounds = font.getStringBoundingBox(countDown, 0, 0);
                    ofTranslate(scrW/2,scrH/2,0);
	    	        font.drawString(countDown, -bounds.width/2, bounds.height/2 );
                    ofTranslate(scrW,0,0);
                    font.drawString(countDown, -bounds.width/2, bounds.height/2 ); 
    	        ofPopMatrix();
            }
            else if(curTime > 10000){
                takeIt = true;
                sStartTime = ofGetElapsedTimeMillis();
            }
        }
    }
    else if(faceFinder.size() == 0 && takeIt == false && showIt == false && slideIt == false){
        startTime = ofGetElapsedTimeMillis();
        sCurTime = ofGetElapsedTimeMillis() - sStartTime;
        if(sCurTime > sTimer*4){
            slideIt = true;
        }
    }
    //Take a screenshot, if the take is activated
    if(slideIt == false && showIt == false && takeIt == true){
        snapshot();
    }
    else if(showIt == true && slideIt == false && takeIt == false){
        showtake();
    }
    else if(slideIt == true && showIt == false && takeIt == false){
        sCurTime = 0;
        //Make an array of file paths of all images in folder
        dir.listDir("/home/susy/Pictures/SuSyMe/");
        dir.allowExt("png");
        dir.sort();
        //Allocate the vector to have as many ofImages as files
        if(dir.size()){
            images.assign(dir.size(),ofImage());
        }
        //You can now iterate through the files and load them into the ofImage vector
        for(int i = 0; i < (int)dir.size(); i++){
            images[i].load(dir.getPath(i));
        }
        slideshow();
    }
}

//--------------------------------------------------------------
void ofApp::snapshot(){
    //Set path and take screenshot
    fileName = "/home/susy/Pictures/SuSyMe/" + ofGetTimestampString() + ".png";
    ofSaveScreen(fileName);
    //Set trigger to false 
    cout<<"Snapped It"<<endl;
    img.load(fileName);
    sStartTime = ofGetElapsedTimeMillis();
    takeIt = false;
    showIt = true;
}


//--------------------------------------------------------------
void ofApp::showtake(){
    //Show snapshot for 10 seconds
    sCurTime = ofGetElapsedTimeMillis() - sStartTime;
    //Show snapped image as long as slide time
    if(sCurTime < sTimer){
        //Draw image
        img.draw(0,0);
        cout<<"Preview Snapshot"<<endl;
    }
    else{
        startTime = ofGetElapsedTimeMillis();
        showIt = false;
    }
}


//--------------------------------------------------------------
void ofApp::slideshow(){
    //Slideshow of captured images as kinda screensaver
    //Collect all files as paths in an array
    sCurTime = ofGetElapsedTimeMillis() - sStartTime;
    //Show snapped image as long as slide time
    if(sCurTime >= 0 && sCurTime < sTimer){
        //Draw image
        images[curImg].draw(0,0);
        //cout<<sCurTime<<endl;
    }
    else if(sCurTime >= sTimer){
        sStartTime=ofGetElapsedTimeMillis();
        curImg=ofRandom(dir.size());
    }
}


//---------------------------------------------------------------

