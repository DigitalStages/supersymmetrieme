# SuperSymmetrieMe

![](images/SusyMe_HW1.JPG)

I believe, this is not only the case for views about shapes and visual marks like the video installation works with, but the perception of inner beauty, in which mental health issues are seen as "asymmetrical", that needs to be medicated (like plastic surgery for the mind). Yes, there are massive injuries, that need to be treated, physical and psychological ones. This wounds are important to heal. However, todays pharmaceutical industry made asymmetries of life look undesirable. A known example here is marketing around Prozac, that deals with the symptom called "Depression". Despressions have in most of the cases a cause, that need to be looked at, but Prozac gives the patient a fast lane back to an artificial balance, or mental symmetry. The drug will keep you away from sadness, anxiety and depression, making you appear more beautiful to others...as long you keep on taking the drug. Fortunately, there are positive signs in society that addresses now this misconception of inner beauty, that expect a form of mental supersymmetry.

Back to facial features and beauty in super-symmetrical faces. Many attempts to explain the pre-conditioned notion of beautiful are only interpretations, like e.g. that we are looking for "supersymmetrical" facial search for a mate by genetic thrives.

This notion has been disproved in a scientific research 2014:

    "Consider the apparently received wisdom that we prefer symmetrical, evenly balanced features. The scientific explanation seems sound: disease and stress during childhood could subtly influence the body’s development, creating an “instability” that leads one side to grow slightly differently to the other. A slightly lopsided face should therefore be a sign of physical weakness – making them less appealing as the parent of your children.

    The problem had been that many of the previous experiments had asked just a small number of subjects to rate different faces – making it easier for fluke results to jump out. When Stefan Van Dongen at the University of Antwerp conflated the results in a large meta-analysis, he found the effect almost disappears when you consider enough people. In fact, facial symmetry may not even say much about your health. Although previous research had found some evidence for the idea, a 2014 study took 3D scans of nearly 5,000 teenagers and quizzed them about their medical history. It found that those with the most symmetrical features had been no fitter than the others.

Reference: BBC "The myth of universal beauty"

In my thoughts, I even went to one conclusion, that this moaning for super-symmetry is a form of the Post-Freudian "Death thrive". The bridge I built is connected to a rather destructive dynamic in the universe. The dynamic that forces everything into perfect symmetries is also called entropy. In example, as soon someone built a house, actually already while building a house, entropy tries to flatten everything back to the ground. The force of gravity, the sun, the rain, the ice and winds work on the house to achieve a sphere (even though Earth is squashed by other forces) to a perfect symmetry. This dynamic in a total playout would eliminate all capabilities of life, which is now predicted by scientist forseeing the death of the Universe.
"Let me see your two faces!"

I had the idea of the software prototype in 2015, when playing the old game of cutting your passport photo in half, and hold each side onto a mirror. The result are 2 supersymmetrical faces, that can differ quite a bit. So I was thinking, that it would be nice to have it as an installation, that shows people their 2 virtual supersymmetrical faces and their real mostly asymmetrical face in a mirror.

Now, 5 years later, I realized the idea to a final result.

![](images/SuSyMe_FeatureImg.png)

#### How does it work?

    If someone steps in front of the camera, the software will cut the recorded face (or faces) in half and create the super-symmetrical versions
    It will count-down with visible numbers on the screen, and make a snapshot
    The snapshop then will be shown to the audience for several seconds
    The snapped photo is added to a photo slide show, which is activated, when nobody is directly in front of the camera, thus causing facial tracking
    The audience can watch the photo slide show of 2 "supersymmetrical" faces of previous participants and can add themselves to the photoslide
    There is also space to experiment doing something unexpected


The installation is made of:

    2 LCD Monitors
    1 Low Profile Computer
    1 Web camera
    Aluminum frame
    Cable extensions and splitter
    1 TV Stand

#### Front view
<br>

![](images/SusyMe_HW2.JPG)


#### Back view
<br>

![](images/SuSyMe_JW3.jpg)

Software

    OS Linux Mint
    Susy Software made with openFrameworks
